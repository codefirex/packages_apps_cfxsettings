package com.codefirex.settings.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.UserInfo;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.codefirex.settings.R;
import com.codefirex.settings.CFXPreferenceFragment;
import com.codefirex.settings.SettingsFragment;

public class AppSecurity extends CFXPreferenceFragment implements OnPreferenceChangeListener {

    static final String TAG = AppSecurity.class.getSimpleName();

    private static final String KEY_SMS_SECURITY_CHECK_PREF = "sms_security_check_limit";

    private ContentResolver mCr;
    private PreferenceScreen mPrefSet;

    private ListPreference mSmsSecurityCheck;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private PreferenceScreen createPreferenceHierarchy() {
        PreferenceScreen root = getPreferenceScreen();
        addPreferencesFromResource(R.xml.system_app_security);
        root = getPreferenceScreen();
        mPrefSet = getPreferenceScreen();
        mCr = getContentResolver();

        // Add package manager to check if features are available
        PackageManager pm = getPackageManager();

        boolean isTelephony = pm.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
        if (isTelephony) {
            mSmsSecurityCheck = (ListPreference) root.findPreference(KEY_SMS_SECURITY_CHECK_PREF);
            mSmsSecurityCheck.setOnPreferenceChangeListener(this);
            int smsSecurityCheck = Integer.valueOf(mSmsSecurityCheck.getValue());
            updateSmsSecuritySummary(smsSecurityCheck);
         } else {
            mPrefSet.removePreference(mPrefSet.findPreference(KEY_SMS_SECURITY_CHECK_PREF));
         }
        return root;
    }

    private void updateSmsSecuritySummary(int i) {
        String message = getString(R.string.sms_security_check_limit_summary, i);
        mSmsSecurityCheck.setSummary(message);
    }

    public boolean onPreferenceChange(Preference preference, Object value) {
        if (preference == mSmsSecurityCheck) {
            int smsSecurityCheck = Integer.valueOf((String) value);
            Settings.Global.putInt(getContentResolver(), Settings.Global.SMS_OUTGOING_CHECK_MAX_COUNT,
                     smsSecurityCheck);
            updateSmsSecuritySummary(smsSecurityCheck);
        }
        return true;
    }
}
