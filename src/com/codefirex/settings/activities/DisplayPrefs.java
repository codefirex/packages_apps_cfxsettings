package com.codefirex.settings.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;
import android.app.Dialog;
import android.os.SystemProperties;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.Preference.OnPreferenceChangeListener;
import android.provider.Settings;
import android.text.Spannable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import com.codefirex.settings.activities.AutoBrightnessCustomizeDialog;

import com.codefirex.settings.R;
import com.codefirex.settings.CFXPreferenceFragment;

import java.io.IOException;
import java.util.List;

public class DisplayPrefs extends CFXPreferenceFragment implements OnPreferenceChangeListener {

    private final static String TAG = DisplayPrefs.class.getSimpleName();

    private static final String KEY_DUAL_PANE = "dual_pane";
    private static final String KEY_USER_MODE_UI = "user_mode_ui";
    private static final String KEY_HIDE_EXTRAS = "hide_extras";
    private ContentResolver mCr;
    private PreferenceScreen mPrefSet;

    private boolean isCrtOffChecked = false;

    private CheckBoxPreference mDualPane;
    private CheckBoxPreference mHideExtras;
    private CheckBoxPreference mCrtOff;
    private CheckBoxPreference mCrtOn;

    private ListPreference mUserModeUI;
    private ListPreference mPluggedTimeout;
    private Preference mCustomBL;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.display_settings);

        ContentResolver cr = mContext.getContentResolver();
        mPrefSet = getPreferenceScreen();
        mCr = getContentResolver();

        mCustomBL = mPrefSet.findPreference("custombl");
        
        /* Dual pane toggle */
        mDualPane = (CheckBoxPreference) mPrefSet.findPreference(KEY_DUAL_PANE);
        mDualPane.setChecked(Settings.System.getInt(mCr,
                Settings.System.DUAL_PANE_SETTINGS, 0) == 1);

        /* UI mode */
        mUserModeUI = (ListPreference) findPreference(KEY_USER_MODE_UI);
        int uiMode = Settings.System.getInt(cr,
                Settings.System.CURRENT_UI_MODE, 0);
        mUserModeUI.setValue(Integer.toString(Settings.System.getInt(cr,
                Settings.System.USER_UI_MODE, uiMode)));
        mUserModeUI.setOnPreferenceChangeListener(this);

        /* Hide compatibility mode */
        mHideExtras = (CheckBoxPreference) findPreference(KEY_HIDE_EXTRAS);
        mHideExtras.setChecked(Settings.System.getBoolean(mCr,
                Settings.System.HIDE_EXTRAS_SYSTEM_BAR, false));

        /* Screen off timeout while plugged in */
        mPluggedTimeout = (ListPreference) findPreference("plugged_timeout");
        mPluggedTimeout.setValue(Long.toString(Settings.System.getLong(mCr, android.provider.Settings.System.SCREEN_OFF_CHARGING_TIMEOUT,
                -1)));
	mPluggedTimeout.setSummary(mPluggedTimeout.getEntry());
        mPluggedTimeout.setOnPreferenceChangeListener(this);

        /* Toggle CRT on and off animations */
        // respect device default configuration
        // true fades while false animates
        boolean electronBeamFadesConfig = mContext.getResources().getBoolean(
                com.android.internal.R.bool.config_animateScreenLights);

        // use this to enable/disable crt on feature
        // crt only works if crt off is enabled
        // total system failure if only crt on is enabled
        isCrtOffChecked = Settings.System.getInt(cr,
                Settings.System.SYSTEM_POWER_ENABLE_CRT_OFF,
                electronBeamFadesConfig ? 0 : 1) == 1;

        mCrtOff = (CheckBoxPreference) findPreference("eos_system_power_crt_screen_off");
        mCrtOff.setChecked(isCrtOffChecked);
        mCrtOff.setOnPreferenceChangeListener(this);

        if(mCrtOn != null) {
            mCrtOn = (CheckBoxPreference) findPreference("eos_system_power_crt_screen_on");
            mCrtOn.setChecked(Settings.System.getInt(cr,
                    Settings.System.SYSTEM_POWER_ENABLE_CRT_ON, 0) == 1);
            mCrtOn.setEnabled(isCrtOffChecked);
            mCrtOn.setOnPreferenceChangeListener(this);
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        boolean value;
        if (preference == mDualPane) {
            value = mDualPane.isChecked();
            Settings.System.putInt(mCr, Settings.System.DUAL_PANE_SETTINGS,
                    value ? 1 : 0);
            return true;
        } else if (preference == mHideExtras) {
            Settings.System.putBoolean(mContext.getContentResolver(),
                    Settings.System.HIDE_EXTRAS_SYSTEM_BAR,
                    ((CheckBoxPreference) preference).isChecked());
            return true;
        } else if (preference == mCustomBL) {
            Dialog d = new AutoBrightnessCustomizeDialog(getActivity());
            d.show();
            return true;
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        if (preference == mUserModeUI) {
            Settings.System.putInt(getActivity().getContentResolver(),
                    Settings.System.USER_UI_MODE, Integer.parseInt((String) newValue));
            return true;
        } else if (preference == mPluggedTimeout) {
	    String val = (String) newValue;
	    Settings.System.putInt(getActivity().getContentResolver(), android.provider.Settings.System.SCREEN_OFF_CHARGING_TIMEOUT, Integer.parseInt(val));
	    int index = mPluggedTimeout.findIndexOfValue(val);
            mPluggedTimeout.setSummary(mPluggedTimeout.getEntries()[index]);
	    return true;
        } else if (preference == mCrtOff) {
            isCrtOffChecked = ((Boolean) newValue).booleanValue();
            Settings.System.putInt(mContext.getContentResolver(),
                    Settings.System.SYSTEM_POWER_ENABLE_CRT_OFF, (isCrtOffChecked ? 1 : 0));
            // if crt off gets turned off, crt on gets turned off and disabled
            if (!isCrtOffChecked) {
                Settings.System.putInt(mContext.getContentResolver(), 
                Settings.System.SYSTEM_POWER_ENABLE_CRT_ON, 0);
                if(mCrtOn != null) {
                    mCrtOn.setChecked(false);
                }
            }
            if(mCrtOn != null) {
                mCrtOn.setEnabled(isCrtOffChecked);
            }
            return true;
        } else if (preference == mCrtOn) {
            Settings.System.putInt(mContext.getContentResolver(), 
                    Settings.System.SYSTEM_POWER_ENABLE_CRT_ON, ((Boolean) newValue).booleanValue() ? 1 : 0);
            return true;
        }
        return false;
    }
}
