package com.codefirex.settings.activities;

import android.content.ContentResolver;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.SystemProperties;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.util.Log;

import com.codefirex.settings.R;
import com.codefirex.settings.SettingsFragment;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.provider.Settings.System.KEYLIGHT_TIMEOUT;
import static android.provider.Settings.System.SCREEN_OFF_TIMEOUT;

public class InterfaceButtons extends SettingsFragment
    implements Preference.OnPreferenceChangeListener {

    private final static String TAG = InterfaceButtons.class.getSimpleName();

    private static final int FALLBACK_KEYLIGHT_TIMEOUT_VALUE = -1;

    private static final String VOLUME_WAKE_TOGGLE = "pref_volume_wake_toggle";
    private static final String LOCKSCREEN_MUSIC_CTRL_VOLBTN = "pref_lockscreen_music_controls_volbtn";
    private static final String KEY_KEYLIGHT_TIMEOUT = "keylight_timeout";
    private static final String KILL_APP_LONGPRESS_BACK = "kill_app_longpress_back";
    private static final String KILL_APP_LONGPRESS_BACK_TIMEOUT = "pref_kill_app_longpress_back_timeout";
    private static final String CAT_SOFTKEYS="pref_softkeys";
    private static final String USE_BUTTONBACKLIGHT="pref_softkeys_bln";
    private static final String VOLUME_SUSPEND_TOGGLE="pref_volume_suspend_toggle";
    private static final String VOLUME_KEYS_CONTROL_RING_STREAM = "pref_volume_keys_control_ringer";

    private ContentResolver mCr;
    private PreferenceScreen mPrefSet;
    private boolean mHasButtonBacklight;
    private boolean mKeylightTimeoutEnabledConfig;

    private CheckBoxPreference mTrackballWake;
    private CheckBoxPreference mVolumeWake;
    private CheckBoxPreference mVolumeSuspend;
    private CheckBoxPreference mMusicCtrlVolBtn;
    private CheckBoxPreference mVolKeyControlRingStream;
    private CheckBoxPreference mKillAppBackBtn;
    private CheckBoxPreference mButtonLightNotification;

    private EditTextPreference mKillAppLongpressBackTimeout;

    private ListPreference mKeylightTimeoutPreference;

    private ListPreference remapPlay;
    private ListPreference remapNext;
    private ListPreference remapPrevious;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.interface_buttons);

        final Resources resources = this.getContext().getResources();

        mPrefSet = getPreferenceScreen();
        mCr = getContentResolver();

        mHasButtonBacklight = getResources().getBoolean(com.android.internal.R.bool.config_hasHardwareKeysWithBacklight);

        /* Volume wake pref */
        mVolumeWake = (CheckBoxPreference) mPrefSet.findPreference(VOLUME_WAKE_TOGGLE);
        mVolumeWake.setChecked(Settings.System.getInt(mCr,
                Settings.System.VOLUME_WAKE_SCREEN, 0) == 1);

        mVolumeSuspend = (CheckBoxPreference) mPrefSet.findPreference(VOLUME_SUSPEND_TOGGLE);
        mVolumeSuspend.setChecked(Settings.Secure.getInt(mCr,
                Settings.Secure.VOLUME_SUSPEND, 0) == 1);

        /* Volume button music controls */
        mMusicCtrlVolBtn = (CheckBoxPreference) mPrefSet.findPreference(LOCKSCREEN_MUSIC_CTRL_VOLBTN);
        mMusicCtrlVolBtn.setChecked(Settings.System.getInt(mCr,
                Settings.System.LOCKSCREEN_MUSIC_CONTROLS_VOLBTN, 0) == 1);

        /* Volume buttons control ring volume */
        mVolKeyControlRingStream = (CheckBoxPreference) mPrefSet.findPreference(VOLUME_KEYS_CONTROL_RING_STREAM);
        mVolKeyControlRingStream.setChecked(Settings.System.getInt(mCr,
                Settings.System.VOLUME_KEYS_CONTROL_RING_STREAM, 1) == 0);

        /* Kill app long press pack */
        mKillAppBackBtn = (CheckBoxPreference) mPrefSet.findPreference(KILL_APP_LONGPRESS_BACK);
        mKillAppBackBtn.setChecked(Settings.System.getInt(getContentResolver(),
                Settings.System.KILL_APP_LONGPRESS_BACK, 0) == 1);

        /* Kill App Longpress Back timeout duration pref */
        mKillAppLongpressBackTimeout = (EditTextPreference) mPrefSet.findPreference(KILL_APP_LONGPRESS_BACK_TIMEOUT);
        mKillAppLongpressBackTimeout.setOnPreferenceChangeListener(this);

        if(mHasButtonBacklight){
        /* SoftKeys - BLN */
            mButtonLightNotification = (CheckBoxPreference) mPrefSet.findPreference(USE_BUTTONBACKLIGHT);
            mButtonLightNotification.setChecked(Settings.System.getInt(mCr,
                Settings.System.NOTIFICATION_USE_BUTTON_BACKLIGHT, 0) == 1);
        /* SoftKeys - Keylight timeout */
            mKeylightTimeoutPreference = (ListPreference) findPreference(KEY_KEYLIGHT_TIMEOUT);
            final long currentTimeout = Settings.System.getLong(mCr, KEYLIGHT_TIMEOUT,
                    FALLBACK_KEYLIGHT_TIMEOUT_VALUE);
            mKeylightTimeoutPreference.setValue(String.valueOf(currentTimeout));
            mKeylightTimeoutPreference.setOnPreferenceChangeListener(this);
            disableUnusableTimeouts(mKeylightTimeoutPreference);
            updateTimeoutPreferenceDescription(currentTimeout);
        } else {
            mPrefSet.removePreference(mPrefSet.findPreference(CAT_SOFTKEYS));
        }


        remapPlay = (ListPreference) mPrefSet.findPreference("pref_remap_play");
        remapPlay.setValue(String.valueOf(Settings.Secure.getInt(mCr, Settings.Secure.REMAP_MEDIA_PLAYPAUSE, 0)));
		remapPlay.setSummary(remapPlay.getEntry());
        remapPlay.setOnPreferenceChangeListener(this);

        remapNext = (ListPreference) mPrefSet.findPreference("pref_remap_next");
        remapNext.setValue(String.valueOf(Settings.Secure.getInt(mCr, Settings.Secure.REMAP_MEDIA_NEXT, 0)));
		remapNext.setSummary(remapNext.getEntry());
        remapNext.setOnPreferenceChangeListener(this);

        remapPrevious = (ListPreference) mPrefSet.findPreference("pref_remap_previous");
        remapPrevious.setValue(String.valueOf(Settings.Secure.getInt(mCr, Settings.Secure.REMAP_MEDIA_PREVIOUS, 0)));
		remapPrevious.setSummary(remapPrevious.getEntry());
        remapPrevious.setOnPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        // This permanently sets the summary in english & makes the definition in strings.xml useless.. should probably fix
        mKillAppLongpressBackTimeout.setSummary(mKillAppLongpressBackTimeout.getContext().getString(
                    R.string.pref_kill_app_longpress_back_summary_alt, mKillAppLongpressBackTimeout.getText()));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void updateTimeoutPreferenceDescription(long currentTimeout) {
        ListPreference preference = mKeylightTimeoutPreference;
        String summary = "";
        if (currentTimeout == -1) {
            summary = preference.getContext().getString(R.string.keylight_timeout_summary_dim);
        } else if (currentTimeout == 0) {
            summary = preference.getContext().getString(R.string.keylight_timeout_summary_disabled);
        } else {
            final CharSequence[] entries = preference.getEntries();
            final CharSequence[] values = preference.getEntryValues();
            int best = 0;
            for (int i = 0; i < values.length; i++) {
                long timeout = Long.parseLong(values[i].toString());
                if (currentTimeout >= timeout) {
                    best = i;
                }
            }
            summary = preference.getContext().getString(R.string.keylight_timeout_summary,
                    entries[best]);
        }
        preference.setSummary(summary);
    }

    private void disableUnusableTimeouts(ListPreference keylightTimeoutPreference) {
        long maxTimeout = Settings.System.getLong(mCr, SCREEN_OFF_TIMEOUT, 30000);
        final CharSequence[] entries = keylightTimeoutPreference.getEntries();
        final CharSequence[] values = keylightTimeoutPreference.getEntryValues();
        ArrayList<CharSequence> revisedEntries = new ArrayList<CharSequence>();
        ArrayList<CharSequence> revisedValues = new ArrayList<CharSequence>();
        for (int i = 0; i < values.length; i++) {
            long timeout = Long.parseLong(values[i].toString());
            if (timeout <= maxTimeout) {
                revisedEntries.add(entries[i]);
                revisedValues.add(values[i]);
            }
        }
        if (revisedEntries.size() != entries.length || revisedValues.size() != values.length) {
            keylightTimeoutPreference.setEntries(
                    revisedEntries.toArray(new CharSequence[revisedEntries.size()]));
            keylightTimeoutPreference.setEntryValues(
                    revisedValues.toArray(new CharSequence[revisedValues.size()]));
            final int userPreference = Integer.parseInt(keylightTimeoutPreference.getValue());
            if (userPreference <= maxTimeout) {
                keylightTimeoutPreference.setValue(String.valueOf(userPreference));
            } else {
                // There will be no highlighted selection since nothing in the list matches
                // maxTimeout. The user can still select anything less than maxTimeout.
                // TODO: maybe append maxTimeout to the list and mark selected.
            }
        }
        keylightTimeoutPreference.setEnabled(revisedEntries.size() > 0);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();

        if (KILL_APP_LONGPRESS_BACK_TIMEOUT.equals(key)) {
            try {
                int timeout = Integer.parseInt((String) newValue);
                if (timeout < 500 || timeout > 2000) {
                    // Out of bounds, bail!
                    return false;
                }
                Settings.System.putInt(mCr, KILL_APP_LONGPRESS_BACK_TIMEOUT, timeout);
                mKillAppLongpressBackTimeout.setSummary(mKillAppLongpressBackTimeout.getContext().getString(
                            R.string.pref_kill_app_longpress_back_summary_alt, timeout));
                mKillAppLongpressBackTimeout.setText(Integer.toString(timeout));
            } finally {
                Log.d(TAG, "Exception error on preference change.");
                return false;
            }
        } else if (KEY_KEYLIGHT_TIMEOUT.equals(key)) {
            int value = Integer.parseInt((String) newValue);
            try {
                Settings.System.putInt(getContentResolver(), KEYLIGHT_TIMEOUT, value);
                updateTimeoutPreferenceDescription(value);
            } catch (NumberFormatException e) {
                Log.e(TAG, "could not persist keylight timeout setting", e);
            }
        } else if (preference == remapPrevious) {
			String val = (String) newValue;
            Settings.Secure.putInt(mCr, Settings.Secure.REMAP_MEDIA_PREVIOUS,
                    Integer.valueOf(val));

			int index = remapPrevious.findIndexOfValue(val);
            remapPrevious.setSummary(remapPrevious.getEntries()[index]);
            return true;
		} else if (preference == remapNext) {
			String val2 = (String) newValue;
            Settings.Secure.putInt(mCr, Settings.Secure.REMAP_MEDIA_NEXT,
                    Integer.valueOf(val2));

			int index2 = remapNext.findIndexOfValue(val2);
            remapNext.setSummary(remapNext.getEntries()[index2]);
            return true;
		} else if (preference == remapPlay) {
			String val3 = (String) newValue;
            Settings.Secure.putInt(mCr, Settings.Secure.REMAP_MEDIA_PLAYPAUSE,
                    Integer.valueOf(val3));

			int index3 = remapPrevious.findIndexOfValue(val3);
            remapPlay.setSummary(remapPlay.getEntries()[index3]);
            return true;
		}
        return true;
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        boolean value;
        if (preference == mVolumeWake) {
            value = mVolumeWake.isChecked();
            Settings.System.putInt(mCr, Settings.System.VOLUME_WAKE_SCREEN,
                    value ? 1 : 0);
            return true;
        } else if (preference == mVolumeSuspend) {
            value = mVolumeSuspend.isChecked();
            Settings.Secure.putInt(mCr, Settings.Secure.VOLUME_SUSPEND,
                    value ? 1 : 0);
            return true;
        } else if (preference == mMusicCtrlVolBtn) {
            value = mMusicCtrlVolBtn.isChecked();
            Settings.System.putInt(mCr, Settings.System.LOCKSCREEN_MUSIC_CONTROLS_VOLBTN,
                    value ? 1 : 0);
            return true;
        } else if (preference == mKillAppBackBtn) {
            value = mKillAppBackBtn.isChecked();
            Settings.System.putInt(mCr, Settings.System.KILL_APP_LONGPRESS_BACK,
                    value ? 1 : 0);
            return true;
        } else if (preference == mVolKeyControlRingStream) {
            value = mVolKeyControlRingStream.isChecked();
            Settings.System.putInt(mCr, Settings.System.VOLUME_KEYS_CONTROL_RING_STREAM,
                    value ? 1 : 0);
            return true;
        } else if (preference == mButtonLightNotification) {
            value = mButtonLightNotification.isChecked();
            Settings.System.putInt(mCr, Settings.System.NOTIFICATION_USE_BUTTON_BACKLIGHT,
                    value ? 1 : 0);
            return true;
        }
        return super.onPreferenceTreeClick(preferenceScreen, preference);
    }

}
