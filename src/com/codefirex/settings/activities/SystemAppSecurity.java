package com.codefirex.settings.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceGroup;
import android.preference.PreferenceScreen;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.codefirex.settings.R;
import com.codefirex.settings.SettingsFragment;

public class SystemAppSecurity extends SettingsFragment implements OnPreferenceChangeListener {

    static final String TAG = SystemAppSecurity.class.getSimpleName();

    private static final String KEY_SMS_SECURITY_CHECK_PREF = "sms_security_check_limit";

    private ListPreference mSmsSecurityCheck;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.system_app_security);
        PreferenceScreen root = getPreferenceScreen();
        root = getPreferenceScreen();

        mSmsSecurityCheck = (ListPreference) findPreference(KEY_SMS_SECURITY_CHECK_PREF);
        mSmsSecurityCheck.setOnPreferenceChangeListener(this);
        int smsSecurityCheck = Integer.valueOf(mSmsSecurityCheck.getValue());
        updateSmsSecuritySummary(smsSecurityCheck);
    }

    private void updateSmsSecuritySummary(int i) {
        String message = getString(R.string.sms_security_check_limit_summary, i);
        mSmsSecurityCheck.setSummary(message);
    }

    public boolean onPreferenceChange(Preference preference, Object value) {
        if (preference == mSmsSecurityCheck) {
            int smsSecurityCheck = Integer.valueOf((String) value);
            Settings.Global.putInt(getContentResolver(), Settings.Global.SMS_OUTGOING_CHECK_MAX_COUNT,
                     smsSecurityCheck);
            updateSmsSecuritySummary(smsSecurityCheck);
        }
        return true;
    }
}
